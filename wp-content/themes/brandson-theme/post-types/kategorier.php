<?php

function kategori_init()
{
    register_post_type('Kategorier', array(
        'labels' => array(
            'name' => __('Kategorier', 'brandson-theme'),
            'singular_name' => __('Kategorier', 'brandson-theme'),
            'all_items' => __('Alla kategorier', 'brandson-theme'),
            'new_item' => __('Ny kategori', 'brandson-theme'),
            'add_new' => __('Lägg till nytt', 'brandson-theme'),
            'add_new_item' => __('Lägg till ny kategori', 'brandson-theme'),
            'edit_item' => __('Redigera kategori', 'brandson-theme'),
            'view_item' => __('Visa kategori', 'brandson-theme'),
            'search_items' => __('Sök kategori', 'brandson-theme'),
            'not_found' => __('Inga kategorier hittade', 'brandson-theme'),
            'not_found_in_trash' => __('Inga kategorier hittade', 'brandson-theme'),
            'parent_item_colon' => __('Förälder kategori', 'brandson-theme'),
            'menu_name' => __('Kategorier', 'brandson-theme'),
        ),
        'public' => true,
        'hierarchical' => false,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'supports' => array('title', 'editor'),
        'has_archive' => true,
        'rewrite' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-portfolio',
        'show_in_rest' => true,
        'rest_base' => 'Kategori',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports' => array('title', 'thumbnail', 'excerpt'),   
    ));

}
add_action('init', 'kategori_init');

function kategori_updated_messages($messages)
{ 
    global $post;

    $permalink = get_permalink($post);

    $messages['kategori'] = array(
        0 => '', // Unused. Messages start at index 1.
        1 => sprintf(__('Kategori uppdaterad. <a target="_blank" href="%s">Visa kategori</a>', 'brandson-theme'), esc_url($permalink)),
        2 => __('Custom field updated.', 'brandson-theme'),
        3 => __('Custom field deleted.', 'brandson-theme'),
        4 => __('Kategori uppdaterad.', 'brandson-theme'),
        /* translators: %s: date and time of the revision */
        5 => isset($_GET['revision']) ? sprintf(__('Kategori återställd till revision från %s', 'brandson-theme'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
        6 => sprintf(__('Kategori publicerat. <a href="%s">Visa kategori</a>', 'brandson-theme'), esc_url($permalink)),
        7 => __('Kategori sparad.', 'brandson-theme'),
        8 => sprintf(__('Kategori skickad. <a target="_blank" href="%s">Visa rapport</a>', 'brandson-theme'), esc_url(add_query_arg('preview', 'true', $permalink))),
        9 => sprintf(__('Kategori schemalagd för: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Förhandsvida rapport</a>', 'brandson-theme'),
            // translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
            date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url($permalink)),
        10 => sprintf(__('Kategori utkast uppdaterad. <a target="_blank" href="%s">Förhandsvisa rapport</a>', 'brandson-theme'), esc_url(add_query_arg('preview', 'true', $permalink))),
    );

    return $messages;
}
add_filter('post_updated_messages', 'rapport_updated_messages');
