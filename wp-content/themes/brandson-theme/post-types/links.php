<?php

function link_init()
{
    register_post_type('Links', array(
        'labels' => array(
            'name' => __('Länkar', 'brandson-theme'),
            'singular_name' => __('Länkar', 'brandson-theme'),
            'all_items' => __('Alla Länkar', 'brandson-theme'),
            'new_item' => __('Ny länk', 'brandson-theme'),
            'add_new' => __('Lägg till nytt', 'brandson-theme'),
            'add_new_item' => __('Lägg till ny länk', 'brandson-theme'),
            'edit_item' => __('Redigera länk', 'brandson-theme'),
            'view_item' => __('Visa länk', 'brandson-theme'),
            'search_items' => __('Sök länk', 'brandson-theme'),
            'not_found' => __('Inga Länkar hittade', 'brandson-theme'),
            'not_found_in_trash' => __('Inga Länkar hittade', 'brandson-theme'),
            'parent_item_colon' => __('Förälder länk', 'brandson-theme'),
            'menu_name' => __('Länkar', 'brandson-theme'),
        ),
        'public' => true,
        'hierarchical' => false,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'supports' => array('title', 'editor'),
        'has_archive' => true,
        'rewrite' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-portfolio',
        'show_in_rest' => true,
        'rest_base' => 'link',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports' => array('title', 'thumbnail', 'excerpt'),   
    ));

}
add_action('init', 'link_init');

function link_updated_messages($messages)
{ 
    global $post;

    $permalink = get_permalink($post);

    $messages['link'] = array(
        0 => '', // Unused. Messages start at index 1.
        1 => sprintf(__('länk uppdaterad. <a target="_blink" href="%s">Visa länk</a>', 'brandson-theme'), esc_url($permalink)),
        2 => __('Custom field updated.', 'brandson-theme'),
        3 => __('Custom field deleted.', 'brandson-theme'),
        4 => __('link uppdaterad.', 'brandson-theme'),
        /* translators: %s: date and time of the revision */
        5 => isset($_GET['revision']) ? sprintf(__('länk återställd till revision från %s', 'brandson-theme'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
        6 => sprintf(__('länk publicerat. <a href="%s">Visa link</a>', 'brandson-theme'), esc_url($permalink)),
        7 => __('länk sparad.', 'brandson-theme'),
        8 => sprintf(__('länk skickad. <a target="_blink" href="%s">Visa rapport</a>', 'brandson-theme'), esc_url(add_query_arg('preview', 'true', $permalink))),
        9 => sprintf(__('länk schemalagd för: <strong>%1$s</strong>. <a target="_blink" href="%2$s">Förhandsvida rapport</a>', 'brandson-theme'),
            // translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
            date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url($permalink)),
        10 => sprintf(__('link utkast uppdaterad. <a target="_blink" href="%s">Förhandsvisa rapport</a>', 'brandson-theme'), esc_url(add_query_arg('preview', 'true', $permalink))),
    );

    return $messages;
}
add_filter('post_updated_messages', 'rapport_updated_messages');
