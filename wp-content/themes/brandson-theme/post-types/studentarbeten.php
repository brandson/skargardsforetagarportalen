<?php

function studentarbeten_init()
{
    register_post_type('Studentarbeten', array(
        'labels' => array(
            'name' => __('Studentarbeten', 'brandson-theme'),
            'singular_name' => __('Studentarbeten', 'brandson-theme'),
            'all_items' => __('Alla studentarbeten', 'brandson-theme'),
            'new_item' => __('Ny studentarbeten', 'brandson-theme'),
            'add_new' => __('Lägg till nytt', 'brandson-theme'),
            'add_new_item' => __('Lägg till ny studentarbeten', 'brandson-theme'),
            'edit_item' => __('Redigera studentarbeten', 'brandson-theme'),
            'view_item' => __('Visa studentarbeten', 'brandson-theme'),
            'search_items' => __('Sök studentarbeten', 'brandson-theme'),
            'not_found' => __('Inga studentarbeten hittade', 'brandson-theme'),
            'not_found_in_trash' => __('Inga studentarbeten hittade', 'brandson-theme'),
            'parent_item_colon' => __('Förälder studentarbeten', 'brandson-theme'),
            'menu_name' => __('Studentarbeten', 'brandson-theme'),
        ),
        'public' => true,
        'hierarchical' => false,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'supports' => array('title', 'editor'),
        'has_archive' => true,
        'rewrite' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-portfolio',
        'show_in_rest' => true,
        'rest_base' => 'Studentarbeten',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    ));

}
add_action('init', 'studentarbeten_init');

function studentarbeten_updated_messages($messages)
{ 
    global $post;

    $permalink = get_permalink($post);

    $messages['studentarbeten'] = array(
        0 => '', // Unused. Messages start at index 1.
        1 => sprintf(__('Studentarbeten uppdaterad. <a target="_blank" href="%s">Visa studentarbeten</a>', 'brandson-theme'), esc_url($permalink)),
        2 => __('Custom field updated.', 'brandson-theme'),
        3 => __('Custom field deleted.', 'brandson-theme'),
        4 => __('Studentarbeten uppdaterad.', 'brandson-theme'),
        /* translators: %s: date and time of the revision */
        5 => isset($_GET['revision']) ? sprintf(__('Studentarbeten återställd till revision från %s', 'brandson-theme'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
        6 => sprintf(__('Studentarbeten publicerat. <a href="%s">Visa studentarbeten</a>', 'brandson-theme'), esc_url($permalink)),
        7 => __('Studentarbeten sparad.', 'brandson-theme'),
        8 => sprintf(__('Studentarbeten skickad. <a target="_blank" href="%s">Visa rapport</a>', 'brandson-theme'), esc_url(add_query_arg('preview', 'true', $permalink))),
        9 => sprintf(__('Studentarbeten schemalagd för: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Förhandsvida rapport</a>', 'brandson-theme'),
            // translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
            date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url($permalink)),
        10 => sprintf(__('Studentarbeten utkast uppdaterad. <a target="_blank" href="%s">Förhandsvisa rapport</a>', 'brandson-theme'), esc_url(add_query_arg('preview', 'true', $permalink))),
    );

    return $messages;
}
add_filter('post_updated_messages', 'rapport_updated_messages');
