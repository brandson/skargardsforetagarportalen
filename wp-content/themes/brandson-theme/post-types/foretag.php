<?php

function foretag_init()
{
    register_post_type('Foretag', array(
        'labels' => array(
            'name' => __('Företagsutveckling', 'brandson-theme'),
            'singular_name' => __('Företagsutveckling', 'brandson-theme'),
            'all_items' => __('Alla företag', 'brandson-theme'),
            'new_item' => __('Nytt företag', 'brandson-theme'),
            'add_new' => __('Lägg till nytt', 'brandson-theme'),
            'add_new_item' => __('Lägg till nytt företag', 'brandson-theme'),
            'edit_item' => __('Redigera företag', 'brandson-theme'),
            'view_item' => __('Visa företag', 'brandson-theme'),
            'search_items' => __('Sök företag', 'brandson-theme'),
            'not_found' => __('Inga företag hittade', 'brandson-theme'),
            'not_found_in_trash' => __('Inga företag hittade', 'brandson-theme'),
            'parent_item_colon' => __('Förälder företag', 'brandson-theme'),
            'menu_name' => __('Företagsutv', 'brandson-theme'),
        ),
        'public' => true,
        'hierarchical' => false,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'supports' => array('title', 'editor'),
        'has_archive' => true,
        'rewrite' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-portfolio',
        'show_in_rest' => true,
        'rest_base' => 'link',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports' => array('title', 'thumbnail', 'excerpt'),   
    ));

}
add_action('init', 'foretag_init');

function foretag_updated_messages($messages)
{ 
    global $post;

    $permalink = get_permalink($post);

    $messages['foretag'] = array(
        0 => '', // Unused. Messages start at index 1.
        1 => sprintf(__('företag uppdaterad. <a target="_blink" href="%s">Visa företag</a>', 'brandson-theme'), esc_url($permalink)),
        2 => __('Custom field updated.', 'brandson-theme'),
        3 => __('Custom field deleted.', 'brandson-theme'),
        4 => __('företag uppdaterad.', 'brandson-theme'),
        /* translators: %s: date and time of the revision */
        5 => isset($_GET['revision']) ? sprintf(__('företag återställd till revision från %s', 'brandson-theme'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
        6 => sprintf(__('företag publicerat. <a href="%s">Visa link</a>', 'brandson-theme'), esc_url($permalink)),
        7 => __('företag sparad.', 'brandson-theme'),
        8 => sprintf(__('företag skickad. <a target="_blink" href="%s">Visa rapport</a>', 'brandson-theme'), esc_url(add_query_arg('preview', 'true', $permalink))),
        9 => sprintf(__('företag schemalagd för: <strong>%1$s</strong>. <a target="_blink" href="%2$s">Förhandsvida rapport</a>', 'brandson-theme'),
            // translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
            date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url($permalink)),
        10 => sprintf(__('företag utkast uppdaterad. <a target="_blink" href="%s">Förhandsvisa företag</a>', 'brandson-theme'), esc_url(add_query_arg('preview', 'true', $permalink))),
    );

    return $messages;
}
add_filter('post_updated_messages', 'foretag_updated_messages');
