<!-- Footer -->
<footer class="footer pt-4 mt-4 text-center text-md-left">

    <!-- Footer Links -->
    <div class="container">
        <div class="row">
        <div class="col-12 d-inline-block">
            <?php 
				// check if the repeater field has rows of data
				if( have_rows('footer-logo-repeater') ):

					// loop through the rows of data
				while ( have_rows('footer-logo-repeater') ) : the_row();

					// display a sub field value
                    $icons = get_sub_field('footer-logo');
                   
					 ?>
            
                <img class="img-fluid wow fadeInDown mb-4 mx-auto p-2" data-wow-delay="0.3s" src="<?php echo $icons;?>" alt="Logo">

           



            <?php
				endwhile;

				else :

				// no rows found

				endif;
				?>
            <!-- First column -->
            </div>
            </div>
            </div>

            <!-- First column -->

            <?php wp_footer();?>

</footer>

<!-- Footer -->

<!--  SCRIPTS  -->

<!-- MDBootstrap Datatables  -->
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/js/addons/datatables.min.js"></script>

<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/js/addons/dataTables.responsive.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dtBasicExample').DataTable({
            "pageLength": 15,
            responsive: true,
            "language": {
                "lengthMenu": "Visa _MENU_ länkar per sida",
                "sSearch": "Sök",
                "zeroRecords": "Inget hittades - tyvärr",
                "info": "Visar sida _PAGE_ av _PAGES_",
                "infoEmpty": "Inga länkar hittades",
                "infoFiltered": "(Filtrerat från _MAX_ länkar)",
                "paginate": {
                    "previous": "Föregående",
                    "next": "Nästa"
                }
            }
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>

<script>
    new WOW().init();

    // MDB Lightbox Init
    $(function () {
        $("#mdb-lightbox-ui").load(
            "<?php echo esc_url(get_template_directory_uri()); ?>/mdb-addons/mdb-lightbox-ui.html");
    });
</script>

</html>