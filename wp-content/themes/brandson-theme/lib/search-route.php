<?php

add_action('rest_api_init', 'skargardSok');

function skargardSok() {
    register_rest_route('skargardsforetagarportalen/v1', 'search', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'skargardsforetagarportalenSearchResults'
    ));
}

function skargardsforetagarportalenSearchResults($data) {
    
    
    $mainQuery = new WP_Query(array(
        'post_type' => array('Forskningar', 'Projekten', 'Rapporter', 'Studentarbeten'),
        'posts_per_page' => -1,
        's' => sanitize_text_field($data['term'])
    ));
    
    $results = array(
        'forskningar' => array(),
        'projekten' => array(),
        'rapporter' => array(),
        'studentarbeten' => array()
    );

    while($mainQuery->have_posts()) {
        $mainQuery->the_post();
       
        if (get_post_type() === 'forskningar') {
            
            array_push($results['forskningar'], array(
                'posttype' => get_post_type(),
                'title' => get_the_title(),
                'content' => get_the_content(),
                'kategori' => get_field('kategori'),
                'publikation-lank' => get_field('publikation-lank'),
                'publikation-referens' => get_field('publikation-referens'),
                'permalink' => get_the_permalink(),
            ));
        }

        if (get_post_type() == 'projekten') {
            
            array_push($results['projekten'], array(
                'posttype' => get_post_type(),
                'title' => get_the_title(),
                'content' => get_the_content(),
                'kategori' => get_field('kategori'),
                'publikation-lank' => get_field('publikation-lank'),
                'publikation-referens' => get_field('publikation-referens'),
                'permalink' => get_the_permalink(),
            ));
        }

        if (get_post_type() == 'rapporter') {
            array_push($results['rapporter'], array(
                'posttype' => get_post_type(),
                'title' => get_the_title(),
                'content' => get_the_content(),
                'kategori' => get_field('kategori'),
                'publikation-lank' => get_field('publikation-lank'),
                'publikation-referens' => get_field('publikation-referens'),
                'permalink' => get_the_permalink(),
            ));
        }

        if (get_post_type() === 'studentarbeten') {
            
            array_push($results['studentarbeten'], array(
                'posttype' => get_post_type(),
                'title' => get_the_title(),
                'content' => get_the_content(),
                'kategori' => get_field('kategori'),    
                'publikation-lank' => get_field('publikation-lank'),
                'publikation-referens' => get_field('publikation-referens'),
                'permalink' => get_permalink(),
            ));
        }

       

    }

    return $results;
}