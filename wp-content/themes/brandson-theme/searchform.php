               
               <?php

                    global $query_string;

                    wp_parse_str( $query_string, $search_query );
                    $search = new WP_Query( $search_query );

                ?>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-10 col-md-12 col-sm-12 col-12 text-center">
                            <!-- <form role="search" method="get" class="form search-form"> -->
                            <div class="input-group md-form form-sm form-2 pl-0">
                            <input id="searchAll" class="form-control my-0 py-1 lime-border" name="search_text" type="text" value="<?php echo get_search_query(); ?>" placeholder="Sök på publikationer" aria-label="Search">
                    

                        <div class="input-group-append">
                            <span class="input-group-text lime lighten-2" id="basic-text1"><i class="fa fa-search text-grey" aria-hidden="true"></i></span>
                        </div>
                        </div> 
                        </div>
                    </div> 
                </div>
                    <?php if (! is_front_page()) : ?>
    
<!-- TYP AV KÄLLOR START-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                            <h4 class="text-white text-uppercase mb-3">Typ av källor:</h4> 
                            <div class="intro-info-content__hr mb-3 hr d-lg-block d-none d-md-block d-sm-block"></div>   
                                <!-- Checkbox 1 -->
                                <div class="form-check form-check-inline">
                                <input type="checkbox" class="form-check-input posttype-checkbox" name="post_type[]" value="Rapporter" id="materialInline1">
                                <label class="form-check-label text-uppercase" for="materialInline1">Offentliga publikationer</label>
                                </div>

                                <!-- Checkbox 2 -->
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input posttype-checkbox" name="post_type[]" value="Forskningar" id="materialInline2">
                                    <label class="form-check-label text-uppercase" for="materialInline2">Vetenskapliga arbeten</label>
                                </div>

                                <!-- Checkbox 3 -->
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input posttype-checkbox" name="post_type[]" value="Studentarbeten" id="materialInline3">
                                    <label class="form-check-label text-uppercase" for="materialInline3">Studentarbeten</label>
                                </div>
                                <!-- Checkbox 4 -->
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input posttype-checkbox" name="post_type[]" value="Projekten" id="materialInline4">
                                    <label class="form-check-label text-uppercase" for="materialInline4">Projektets publikationer</label>
                                </div>
                                <div class="intro-info-content__hr mb-3 mt-3 hr d-lg-block d-none d-md-block d-sm-block"></div>  
                        </div>
  <!-- TYP AV KÄLLOR END -->                      
                        
            
            <!-- KATEGORIER START -->    

                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <h4 class="text-white text-uppercase mb-3">Kategorier:</h4>
                        <div class="intro-info-content__hr mb-3 mt-3 hr d-lg-block d-none d-md-block d-sm-block"></div>  
                        
                         <?php
        



                        $args = array(
                            'post_type' => 'Kategorier',
                            'posts_per_page' => -1,
                            'order' => 'DESC'                            
                        );
                        
                        $query = new WP_Query($args);
                        $i = 0;
                        while($query -> have_posts()) : $query -> the_post();
                        $i++;
                    ?>

                        <div class="form-check form-check-inline mt-4">
                                    <input type="checkbox" class="form-check-input category-checkbox" name="Kategorier[]" value="<?php echo get_the_ID(); ?>" id="materialInlineCat<?php echo $i;?>">
                                    <label class="form-check-label text-uppercase" for="materialInlineCat<?php echo $i;?>"><?php the_title(); ?></label>
                                </div>
                    
                           
                    <?php endwhile; wp_reset_query(); ?>

                        
                        <div class="intro-info-content__hr mb-3 mt-3 hr d-lg-block d-none d-md-block d-sm-block"></div>  
                        </div> 
                        <?php endif; ?>
                    <!-- </form> -->
                            