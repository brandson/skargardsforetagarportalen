<?php
/**
 * Template Name: Start page
 *
 * @package WordPress
 * @subpackage Brandson Theme
 * @since Brandson Theme 1.0
 */

get_header();?>

<?php $text = get_field('header-text'); ?>

<!-- Intro Section -->
<div id="home" class="view" style="background-image: url('<?php the_post_thumbnail_url()?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <div class="mask rgba-stylish-light">
        <div class="container h-50 d-flex justify-content-center align-items-center">
            <div class="row pt-5 mt-3">
                <div class="col-md-12 mb-3 mx-auto">
                    <div class="intro-info-content text-center">
                        <a href="<?php bloginfo( 'url' ); ?>" class="intro-info-content__standard-logo"><img class="img-fluid wow fadeInDown mb-4"
                                data-wow-delay="0.3s" src="<?php bloginfo( 'template_url' ); ?>/img/skargarden.png" alt="Logo"></a>
                        <div class="col-lg-7 col-md-7 mx-auto">
                            <span class="text-white text-center">
                                <?php echo $text; ?></span>
                        </div>
                        <a href="<?php the_permalink(100); ?>"> <button type="button" class="btn btn-main mt-4 mx-auto">Sök på publikationer</button></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

</header>



<!-- Main Navigation -->
<?php get_template_part('components/firstbox'); ?>
<?php get_template_part('components/secondbox'); ?>
<?php get_template_part('components/thirdbox'); ?>
<?php get_template_part('components/fourthbox'); ?>






<?php get_footer();?>