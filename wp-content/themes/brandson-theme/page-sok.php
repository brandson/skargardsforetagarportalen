<?php
/**
 * Template Name: Sök
 *
 * @package WordPress
 * @subpackage Brandson Theme
 * @since Brandson Theme 1.0
 */

get_header();?>



        <!-- Intro Section -->
        <div id="home" class="view search" style="background-image: url('<?php the_post_thumbnail_url()?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
          <div class="mask rgba-stylish-light">
            <div class="container h-50 d-flex justify-content-center align-items-center">
                <div class="intro-info-content text-center">
                    <div class="row pt-5 mt-3 justify-content-center">
                
                        <div class="col-lg-3 col-md-3 col-sm-auto mb-3">                      
                                <div class="intro-info-content__hr hr"></div>
                                <h1 class="display-3 white-text wow fadeInDown" data-wow-delay="0.3s"><?php the_title()?></h1>
                                <div class="intro-info-content__hr hr"></div>                         
                        </div>  

                        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        
                            <?php get_search_form(); ?>
                        </div> 
                        
                        
                    </div>                               
                </div>
            </div>
          </div>
        </div>

    </header>
    






<div class="searchresult">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-auto col-xs-auto">
            <h4 class="text-uppercase"><i>Sökresultat:</i> "<?php echo $text; ?>"</h4>
             
            </div>
        </div>
    </div>
   
    <div class="container">
        <div id="search-results" class="row">
    
  
        </div>
    </div>
</div>  
   



<?php get_footer();?>






