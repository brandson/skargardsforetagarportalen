(function (root, $, undefined) {
  "use strict";

  $(function () {
    console.log(portalenData.root_url); // Components

    var postTypeCheckbox = $('.posttype-checkbox'),
        categoryCheckbox = $('.category-checkbox'),
        searchAll = $('#searchAll'),
        postType = [],
        category = []; // Key events

    searchAll.on("keyup", startSearch);
    postTypeCheckbox.on("change", getValue);
    categoryCheckbox.on("change", getValue); // Function to get checkbox values and push them into array

    function getValue() {
      var $this = $(this);

      if ($this.hasClass("posttype-checkbox")) {
        postType = [];
        $(".posttype-checkbox:checked").each(function () {
          postType.push($(this).val());
        });
      }

      if ($this.hasClass("category-checkbox")) {
        category = [];
        $(".category-checkbox:checked").each(function () {
          var newVal = Number($(this).val());
          category.push(newVal);
        });
      }
    } // Function to start searching on keydown event


    function startSearch(e) {
      e.preventDefault();
      var inputValue = $(this).val();

      if (e.keyCode == 13) {
        getJSON(inputValue);
      }

      function getJSON() {
        $.getJSON(portalenData.root_url + '/wp-json/skargardsforetagarportalen/v1/search?term=' + inputValue, function (data) {
          $("#search-results").html("");

          if (postType.indexOf() == -1) {// var result = data.filter(item => {
            // 	if(category.length > 0) {
            // 		return category.includes(item.kategori[0]);
            // 	} else {
            // 		return item;
            // 	}
            // });
            // $("#search-results").append(
            // 	data.forEach(function(element) {
            // 	element.map(item => `
            // 	<div class="col-lg-12 col-md-12">
            // 		<div class="container__hr mb-3 mt-3 hr"></div>
            // 		</div>
            // 	  <div class="col-12">
            // 	  <span class="text-uppercase"></span>     
            // 		<h2 class="text-uppercase">${item.title}</h2>
            // 		<div class="container__hr mb-3 mt-3 hr"></div> 
            // 			<p class="text-left">${item.content.slice(0, 240)}</p>
            // 		</div>
            // 	`)
            // 	})
            // );
          }

          if (postType.indexOf("Forskningar") != -1) {
            var result = data.forskningar.filter(item => {
              if (category.length > 0) {
                return category.includes(item.kategori[0]);
              } else {
                return item;
              }
            });
            $("#search-results").append(result.map(item => `
							<div class="col-lg-12 col-md-12">
								<div class="container__hr mb-3 mt-3 hr"></div>
								</div>
	
							  <div class="col-12">
							  <span class="text-uppercase"></span>     
							   
								<h2 class="text-uppercase">${item.title}</h2>
								<div class="container__hr mb-3 mt-3 hr"></div> 
									<p class="text-left">${item.content.slice(0, 240)} <a href="${item.permalink}">Läs mer...</a></p>
								</div>`));
          }

          if (postType.indexOf("Projekten") != -1) {
            var result = data.projekten.filter(item => {
              if (category.length > 0) {
                return category.includes(item.kategori[0]);
              } else {
                return item;
              }
            });
            $("#search-results").append(result.map(item => `
							<div class="col-lg-12 col-md-12">
								<div class="container__hr mb-3 mt-3 hr"></div>
								</div>
	
							  <div class="col-12">
							  <span class="text-uppercase"></span>     
							   
								<h2 class="text-uppercase">${item.title}</h2>
								<div class="container__hr mb-3 mt-3 hr"></div> 
									<p class="text-left">${item.content.slice(0, 240)} <a href="${item.permalink}">Läs mer...</a></p>
								</div>`));
          }

          if (postType.indexOf("Rapporter") != -1) {
            var result = data.rapporter.filter(item => {
              if (category.length > 0) {
                return category.includes(item.kategori[0]);
              } else {
                return item;
              }
            });
            $("#search-results").append(result.map(item => `
							<div class="col-lg-12 col-md-12">
								<div class="container__hr mb-3 mt-3 hr"></div>
								</div>
	
							  <div class="col-12">
							  <span class="text-uppercase"></span>     
							   
								<h2 class="text-uppercase">${item.title}</h2>
								<div class="container__hr mb-3 mt-3 hr"></div> 
									<p class="text-left">${item.content.slice(0, 240)} <a href="${item.permalink}">Läs mer...</a></p>
								</div>`));
          }

          if (postType.indexOf("Studentarbeten") != -1) {
            var result = data.studentarbeten.filter(item => {
              if (category.length > 0) {
                return category.includes(item.kategori[0]);
              } else {
                return item;
              }
            });
            $("#search-results").append(result.map(item => `
							<div class="col-lg-12 col-md-12">
								<div class="container__hr mb-3 mt-3 hr"></div>
								</div>
	
							  <div class="col-12">
							  <span class="text-uppercase"></span>     
							   
								<h2 class="text-uppercase">${item.title}</h2>
								<div class="container__hr mb-3 mt-3 hr"></div> 
									<p class="text-left">${item.content.slice(0, 240)} <a href="${item.permalink}">Läs mer...</a></p>
								</div>`));
          }
        });
      }
    }
  });
})(this, jQuery);