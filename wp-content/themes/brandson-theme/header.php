<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>

	<meta charset="<?php bloginfo('charset');?>">
	<title><?php wp_title('');?><?php if (wp_title('', false)) {echo ' : ';}?><?php bloginfo('name');?></title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">
	<link href="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/favicon.ico" rel="shortcut icon">
	<link href="<?php echo esc_url(get_template_directory_uri()); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name');?>" href="<?php bloginfo('rss2_url');?>" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="<?php bloginfo('description');?>">

	<?php wp_head();?>

    <!--  Font Awesome  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Bootstrap core CSS -->
	<link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap.min.css" rel="stylesheet">

	<!-- Material Design Bootstrap -->
	<link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/mdb.min.css" rel="stylesheet">
	<link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/style.min.css" rel="stylesheet">
    <!-- MDBootstrap Datatables  -->
<link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/addons/datatables.min.css" rel="stylesheet">

</head>
<body class="brandson-skin">
    <!-- Main Navigation -->
    <header>
        

        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar mt-3">
            <div class="container smooth-scroll">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                    <ul class="navbar-nav mr-auto text-uppercase mx-auto">
                        
<?php wp_nav_menu(array(
    'theme_location' => 'main',
    'depth' => 2, // 1 = no dropdowns, 2 = with dropdowns.
    'container' => 'div',
    'container_class' => 'collapse navbar-collapse',
    'container_id' => 'bs-example-navbar-collapse-1',
    'menu_class' => 'navbar-nav mr-auto',
    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
    'walker' => new WP_Bootstrap_Navwalker(),
));?>

                    </ul>
                </div>
            </div>
        </nav>