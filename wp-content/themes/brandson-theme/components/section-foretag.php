<div class="container">
    <div class="row">
        <div class="col-lg-12 col-12">
            <table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" style="width:100%">
                <thead>
                    <tr>
                        <th class="th-sm">Organisation
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Beskrivning
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                        <th class="th-sm">Länk
                            <i class="fa fa-sort float-right" aria-hidden="true"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <tr>
                        <td>
                            <?php the_title(); ?>
                        </td>
                        <td>
                            <?php the_field('beskrivning'); ?>
                        </td>
                        <td>
                            <a href="<?php the_field('lank'); ?>"><?php the_field('lank'); ?></a>
                        </td>
                    </tr>
                    <?php endwhile; endif;?>


                </tbody>
                <tfoot>
                    <tr>
                        <th>Organisation
                        </th>
                        <th>Beskrivning
                        </th>
                        <th>Länk
                        </th>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>