<section id="archive">
    <div class="searchresult">
        <div class="container">
        <?php 
                $ourCurrentPage = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                $customPosts = new WP_Query(array(
                    'paged' => $ourCurrentPage,
                    'post_type' => 'Forskningar', 'Projekten', 'Studentarbeten', 'Rapporter',
                    'posts_per_page' => 4,
                ));
                
             if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="row">


                <div class="col-lg-7 col-md-7 col-sm-12 col-12">                    
                    <h2 class="text-uppercase h2-responsive mt-4">
                        <?php the_title(); ?>
                    </h2>
                
                    <div class="container__hr mb-3 mt-3 hr"></div>
                    <p class="text-left">
                        <?php the_content();?>
                    </p>
                    
                </div>

                <div class="col-lg-5 col-md-5 col-sm-12 col-12 d-lg-flex align-items-center justify-content-start">
                    <div class="mb-4 mt-4">
                    <?php get_template_part("components/sidebar-posttypes");?>
                    </div>
                </div>
            </div>
            <?php endwhile; endif;?>

           <div style="text-align:left;">
            <?php posts_nav_link( ' &#183; ', 'Föregående sida', 'Nästa sida' ); ?>
            </div>
        </div>
    </div>
</section>

