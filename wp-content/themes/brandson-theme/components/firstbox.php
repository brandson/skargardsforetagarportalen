<div class="firstbox">
    <section id="firstbox" class="section wow fadeIn" data-wow-delay="0.3s">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 offset-lg-1">
                    <h2 class="firstbox__h2 h2 text-left text-uppercase mt-5 mb-4 h2-responsive">
                        <?php the_field('firstbox-header'); ?>
                    </h2>
                    <div class="hr">
                    </div>
                    <p class="text-left mt-4">
                        <?php the_field('firstbox-text'); ?>
                    </p>
                    <a href="<?php the_permalink(100); ?>"> <button type="button" class="btn btn-main mx-auto">Sök</button></a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                    <ul class="smooth-scroll list-unstyled">
                        <li>
                            <a href="#scroll1"><img src="<?php the_field('firstbox-icon'); ?>" class="firstbox-icon" />
                            </a>
                        </li>
                    </ul>
                    <img src="<?php the_field('firstbox-img'); ?>" class="firstbox-img" />
                </div>

            </div>
        </div>
        <div id="firstbox-img2" class="view2" style="background-image: url('<?php the_field('firstbox-img2'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
        </div>

    </section>
</div>