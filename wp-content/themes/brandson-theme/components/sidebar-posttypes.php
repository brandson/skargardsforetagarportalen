<!-- Card Regular -->
<div class="card card-cascade" id="publikationer-card">

  <!-- Card content -->
  <div class="card-body card-body-cascade text-center">

    <ul class="list-group list-group-flush">
      <li class="list-group-item">Länk: <a href="<?php the_field('publikation-lank'); ?>">
          <p class="font-italic">
            <?php the_field('publikation-lank'); ?>
          </p>
        </a></li>
      <li class="list-group-item">Referens: <p class="font-italic">
          <?php the_field('publikation-referens'); ?>
        </p>
      </li>

    </ul>

    <a href="<?php the_permalink(100); ?>"> <button type="button" class="btn btn-main mt-4 mx-auto w-auto">Klicka här för att
        komma till sök</button></a>
  </div>

</div>
<!-- Card Regular -->