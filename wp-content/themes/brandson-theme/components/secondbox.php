<div class="secondbox">
    <section id="secondbox" class="section wow fadeIn" data-wow-delay="0.3s">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-12 text-center mx-auto">
                    <img src="<?php the_field('secondbox-img'); ?>" class="secondbox-img" />
                </div>
                <div class="col-lg-6 col-md-7 col-sm-12 col-12 mx-auto">
                    <h2 class="secondbox__h2 h2 text-left text-uppercase mt-5 mb-4 h2-responsive">
                        <?php the_field('secondbox-header'); ?>
                    </h2>
                    <div class="hr">
                    </div>
                    <p class="text-left mt-4">
                        <?php the_field('secondbox-text'); ?>
                    </p>
                    <a href="<?php echo get_post_type_archive_link( 'links' ); ?>"> <button type="button" class="btn btn-main mx-auto">Till länkar</button></a>
                </div>

            </div>
        </div>

    </section>
</div>