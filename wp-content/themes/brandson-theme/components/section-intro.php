<!-- Intro Section -->
<section id="intro">
    <div class="view pages" style="background-image: url('<?php bloginfo( 'template_url' ); ?>/img/background.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
        <div class="mask rgba-stylish-light">
            <div class="container h-50 d-flex justify-content-center align-items-center">
                <div class="row pt-5 mt-3">
                    <div class="col-md-12 mb-3">
                        <div class="intro-info-content text-center">
                            <h1 class="display-3 white-text mb-3 wow fadeInDown h1-responsive" data-wow-delay="0.3s">
                                <?php if (is_archive()) :?>
                                    <h1 class="display-3 white-text mb-3 wow fadeInDown h1-responsive" data-wow-delay="0.3s">
                                        <?php echo str_replace("Arkiv:", "", get_the_archive_title());?>
                                    </h1>
                                <?php else: ?>
                                    <h1 class="display-3 white-text mb-3 wow fadeInDown h1-responsive" data-wow-delay="0.3s">
                                            <?php the_title();?>
                                    </h1>
                                <?php endif;?>

                                <div class="intro-info-content__hr mb-1 hr"></div>
                            </h1>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<ol class="breadcrumbs breadcrumb light-gray"><div class="container">','</div></ol>');
        }
    ?>
</header>