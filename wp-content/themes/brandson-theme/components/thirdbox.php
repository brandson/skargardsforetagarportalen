<div class="thirdbox">
    <section id="thirdbox" class="section wow fadeIn" data-wow-delay="0.3s">
        <div id="thirdbox-img" class="view3" style="background-image: url('<?php the_field('thirdbox-img'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 mr-lg-3">
                        <h2 class="thirdbox__h2 h2 text-left text-uppercase text-white mt-5 mb-4 h2-responsive" id="scroll1">
                            <?php the_field('thirdbox-header'); ?>
                        </h2>
                        <div class="hr">
                        </div>
                        <p class="mt-4 text-white">
                            <?php the_field('thirdbox-text'); ?>
                        </p>
                        <div class="text-left mb-5">
                        <a href="https://karriar.sh.se/sv/for-employers"> <button type="button" class="btn btn-main mt-lg-4 mx-auto">Södertörns högskola</button></a>
                        <a href="https://www.novia.fi/om-oss/karriartjanst-for-foretag/"> <button type="button" class="btn btn-main mt-lg-4 mx-auto">Novia Yrkeshögskola</button></a>
                        <a href="https://www.aarresaari.net//index.php?13&uniid=8&lang_id=4."> <button type="button" class="btn btn-main mt-lg-4 mx-auto">Åbo Akademi</button></a>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>