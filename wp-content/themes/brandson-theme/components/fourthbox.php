            <div class="fourthbox">
                <section id="fourthbox" class="section wow fadeIn" data-wow-delay="0.3s">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <h2 class="text-left mt-5 text-uppercase h2-responsive">
                                    <?php the_field('fourthbox-header'); ?>
                                </h2>
                                <div class="hr mt-3"></div>
                                <p class="text-left mt-5">
                                    <?php the_field('fourthbox-text'); ?>
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <p class="fourthbox__text-left text-left">
                                    <?php the_field('fourthbox-text2'); ?>
                                </p>
                            </div>
                        </div>
                    </div> 
                                 
                </section>
            </div>
