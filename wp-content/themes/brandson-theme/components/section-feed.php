<!-- Section: Magazine v.3 -->

<?php 
    $forskningar = get_posts( array( 'post_type' => 'forskningar', 'posts_per_page' => 5, ) );
    $rapporter = get_posts( array( 'post_type' => 'rapporter', 'posts_per_page' => 5 ) );
    $studentarbeten = get_posts( array( 'post_type' => 'studentarbeten', 'posts_per_page' => 5 ) );
?>

<section id="feed">
    <div class="container">
        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold text-center my-5">Senaste arbeten</h2>
        <div class="hr"></div>
        <!-- Section description -->
        <!-- Grid row -->
        <div class="row">
            <!-- Grid column -->
            <div class="col-lg-4 col-md-12 mb-lg-0 mb-5">
            <?php 
                $first = true;
                foreach ( $forskningar as $forskning ):
                    if ( $first ): $first = false;?>
                        <!-- Featured news -->
                        <div class="single-news mb-3">
                            <!-- Grid row -->
                            <div class="row mb-3">
                            <!-- Grid column -->
                            <div class="col-12">
                                <!-- Badge -->
                                <a href="#!"><span class=" badge green-bg text-center"></i>Forskningar</span></a>
                            </div>
                            <!-- Grid column -->
                            </div>
                            <!-- Grid row -->
                            <!-- Title -->
                            <div class="d-flex justify-content-between">
                            <div class="col-11 text-truncate pl-0 mb-3">
                                <a href="<?php the_permalink($forskning->ID);?>" class="font-weight-bold"><?php echo $forskning->post_title; ?></a>
                            </div>
                            <a><i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <!-- Featured news -->
                    <?php else: ?>
                        <!-- Small news -->
                        <div class="single-news mb-3">
                            <!-- Title -->
                            <div class="d-flex justify-content-between">
                            <div class="col-11 text-truncate pl-0 mb-3">
                                <a href="<?php the_permalink($forskning->ID);?>""><?php echo $forskning->post_title; ?></a>
                            </div>
                            <a><i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <!-- Small news -->
                    <?php endif; ?>
                <?php endforeach;?>
            </div>
            <!-- Grid column -->
            <!-- Grid column -->
            <div class="col-lg-4 col-md-12 mb-lg-0 mb-5">
            <?php 
                $first = true;
                foreach ( $studentarbeten as $studentarbete ):
                    if ( $first ): $first = false;?>
                        <!-- Featured news -->
                        <div class="single-news mb-3">
                            <!-- Grid row -->
                            <div class="row mb-3">
                            <!-- Grid column -->
                            <div class="col-12">
                                <!-- Badge -->
                                <a href="#!"><span class=" badge green-bg text-center"> </i>Studentarbeten</span></a>
                            </div>
                            <!-- Grid column -->
                            </div>
                            <!-- Grid row -->
                            <!-- Title -->
                            <div class="d-flex justify-content-between">
                            <div class="col-11 text-truncate pl-0 mb-3">
                                <a href="<?php the_permalink($studentarbete->ID);?>" class="font-weight-bold"><?php echo $studentarbete->post_title; ?></a>
                            </div>
                            <a><i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <!-- Featured news -->
                    <?php else: ?>
                        <!-- Small news -->
                        <div class="single-news mb-3">
                            <!-- Title -->
                            <div class="d-flex justify-content-between">
                            <div class="col-11 text-truncate pl-0 mb-3">
                                <a href="<?php the_permalink($studentarbete->ID);?>"><?php echo $studentarbete->post_title; ?></a>
                            </div>
                            <a><i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <!-- Small news -->
                    <?php endif; ?>
                <?php endforeach;?>
            </div>
            <!-- Grid column -->
            <!-- Grid column -->
            <div class="col-lg-4 col-md-12 mb-lg-0 mb-5">
            <?php 
                $first = true;
                foreach ( $rapporter as $rapport ):
                    if ( $first ): $first = false;?>
                        <!-- Featured news -->
                        <div class="single-news mb-3">
                            <!-- Grid row -->
                            <div class="row mb-3">
                            <!-- Grid column -->
                            <div class="col-12">
                                <!-- Badge -->
                                <a href="#!"><span class=" badge green-bg text-center"> </i>Rapporter</span></a>
                            </div>
                            <!-- Grid column -->
                            </div>
                            <!-- Grid row -->
                            <!-- Title -->
                            <div class="d-flex justify-content-between">
                            <div class="col-11 text-truncate pl-0 mb-3">
                                <a href="<?php the_permalink($rapport->ID);?>" class="font-weight-bold"><?php echo $rapport->post_title; ?></a>
                            </div>
                            <a><i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <!-- Featured news -->
                    <?php else: ?>
                        <!-- Small news -->
                        <div class="single-news mb-3">
                            <!-- Title -->
                            <div class="d-flex justify-content-between">
                            <div class="col-11 text-truncate pl-0 mb-3">
                                <a href="<?php the_permalink($rapport->ID);?>"><?php echo $rapport->post_title; ?></a>
                            </div>
                            <a><i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                        <!-- Small news -->
                    <?php endif; ?>
                <?php endforeach;?>
            </div>
            <!-- Grid column -->
        </div>
        <!-- Grid row -->
        <div class="row">
            <div class="col-3 mx-auto mb-3">
                <button type="button" class="btn btn-main mt-5 waves-effect waves-light">Sök på fler</button>
            </div>
        </div>
    </div>
</section>
<!-- Section: Magazine v.3 -->