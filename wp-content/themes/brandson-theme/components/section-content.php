<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; endif;?>
            </div>
            <div class="col-lg-4 col-md-4 text-center">
                <?php if (is_single()):?>
                    <?php get_template_part("components/sidebar-posttypes");?>
                <?php else: ?>
                    <div class="img-fluid">
                        <img src="<?php the_field('page-img'); ?>" />
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>